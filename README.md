Use Case : This Asset can be used to catch the error from the backend and Display accordingly through ShowToast method.

Technical Details:  This Function can be used whenever there is an error from the backend. Usually the error from the backend side will be huge to display such as “ Update failed Exceeded with Id…” so we can handle this from our method call in the catch and display the message accordingly by showToast.


For Example:



We get the error message when we try to Update from the backend. This can be Controlled by Using our JS Method in PubSub “getErrorMessage” and Use that in Our Catch method while Fetching or Updating the records.
(here the showToast method is from ShowToastEvent from 'lightning/platformShowToastEvent')


Like : 


          saveRecord({
           })
           .then(result => {
           })
           .catch(error => {
               let errorMsg = getErrorMessage(error);
               this.dispatchEvent(showToast("", errorMsg, "error"));
           });
