const getErrorMessage = (error) => {
	let erroMsg = error.message || error.body.message ||
		(error.body && error.body.pageErrors && error.body.pageErrors[0].message) ||
		(error.body && error.body.fieldErrors && error.body.fieldErrors[0].message);
	if (erroMsg.includes('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
		let erroMsgArray = erroMsg.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
		if (erroMsgArray.length > 1) {
			erroMsg = erroMsgArray[1];
			erroMsg = erroMsg.replace(": []", "");
		}
	}

	return erroMsg;
}
const showToast = (title, message, variant) => {
	const event = new ShowToastEvent({
		title: title,
		message: message,
		variant: variant
	});
	return event;
}

export{
getErrorMessage,
showToast
}